
/*
 * Dendron, a lousy chess engine
 * Copyright 2005, James D. Taylor
 *
 * chessBoard.h
 */

#ifndef CHESS_BOARD_H
#define CHESS_BOARD_H

#include "LossyPriorityQueue.h"
#include "moveLine.h"
#include "chessPiece.h"

using namespace std;
/*
 * dimension of one side of square chess board
 */
#define BOARD_DIMENSION 8
#define BOARD_SIZE BOARD_DIMENSION*BOARD_DIMENSION
#define NPIECE (BOARD_DIMENSION * 2)
#define MAX_TREE_DEPTH 42
//
// King Hostage ID.. meaning one of the kings was captured.
//
#define MOVE_ERROR_KING_HID			-2147483599
#define MOVE_ERROR					-2147483600
#define MOVE_ERROR_OUT_OF_BOUNDS	-2147483601
#define MOVE_ERROR_SRC_INVALID		-2147483602
#define MOVE_ERROR_DST_INVALID		-2147483603


class chessBoard
{
public:
    chessBoard();
    ~chessBoard();
    void resetBoard();
    void clearBoard();
    void deleteBoard();
    int printBoard(FILE *f);
    int setLogFile(FILE *fp);

    void setMaxTreeDepth(unsigned depth);
    unsigned getMaxTreeDepth();
	int setLineCount(unsigned nLine); // how many move lines to track

	void setSelfWhite(bool b); // whether the engine will play white
	void setSelfBlack(bool b); // whether the engine will play black

    int forwardMove(bool bWhite, char *pos); // force user move
	int forwardMove(); // let computer make the move
    int setBaseMove(int nMove);
    char* getLastMove() { return m_strLastMove; }
    bool inCheck(bool bWhite);
    bool inCheckMate() { return m_bCheckMate; }

	unsigned getNumMoves() { return m_nMoves; }

	 // only track lines with this move at 'ply'
	 int watchMove(int ply, char *strMove);
protected:
	int heuristic();
    int findBestLines(int parentRank, bool &parentInCheck);
    int forwardMove(unsigned int &move, bool &seenCheck);
    int reverseMove(unsigned int &move);
	char* move2str(unsigned int move, bool bExtra = true);
	int str2move(unsigned int *move, char *strMove);
    int setLastMove(unsigned int move);
    void printError(int error);
    int dumpMove(unsigned int move);

	unsigned m_nPiece; // number of chess pieces for each side
    chessPiece *m_board[BOARD_SIZE]; // 2d array representation of the board
	chessPiece *m_whites; // array of all the white pieces
	chessPiece *m_blacks; // ... black pieces
    chessPiece *m_jail[32]; // captured piece pointers go here
	boardInfo m_boardInfo; // info structure shared by all chess pieces
    unsigned m_maxTreeDepth; // depth (ply) the engine searches to
    unsigned m_curTreeDepth; // current depth being searched by minimax
    int m_baseMoveNum;	// current move of the game
	bool m_baseMoveWhite; // whether the current move is whites turn
    unsigned int m_nMoves; // total number of moves the engine has calculated
    char m_strLastMove[8];
    FILE *m_fpLog;
	bool m_bCheckMate; // whether current move cannot get out of check
	unsigned m_gameMove; // which move the board is currently set to

	//
	// best move line tracking.  keeps a list of the best lines
	// that the engine has seen while thinking.
	void clearLines();
	void updateBestLine(int curEval);
	moveLine m_curLine;
	//unsigned m_curLineMoves[MAX_TREE_DEPTH];
	//int m_curLineEvals[MAX_TREE_DEPTH];
	unsigned int m_nLine; // # of best lines to keep track of
	//moveLine_t *m_bestLinesBuf;
	LossyPriorityQueue<moveLine> *m_pBestLines;

	unsigned int m_watchPly;
	unsigned int m_watchMove;
private:
};

#endif

