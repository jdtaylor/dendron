
/*
 * Dendron, a lousy chess engine
 * Copyright 2005, James D. Taylor
 *
 * chessBoard.cpp
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <string.h>
#include "chessBoard.h"
#include "chessPiece.h"

#ifndef ASSERT
#include <assert.h>
#define ASSERT(c) assert(c)
#endif

#ifdef _WIN32
#define snprintf _snprintf
#endif

#ifndef NULL
#define NULL 0
#endif

#ifndef MAX_INT
#define MAX_INT 0x7FFFFFFF
#endif

#define MOVE_INVALID 0xFFFFFFFF

/*
 * the queen can move this many possible places
 * its really (n-1)*4 if n is odd
 * and (n-1)*4+3 if n is even. 
 */
#define MAX_PIECE_MOVES (BOARD_DIMENSION*4)

#define MOVE_POS_SRC(a) ((a & 0x00FF0000) >> 16)
#define MOVE_POS_DST(b) ((b & 0xFF000000) >> 24)
#define MOVE_SET_POS(z,a,b) z = ((z & 0x0000FFFF) | ((a << 16) | \
	    			(b << 24) & 0xFFFF0000))
// offset into jail array
#define MOVE_GET_JAIL(a) ((a & 0x0000F800) >> 11)
#define MOVE_SET_JAIL(z,a) z = ((z & 0xFFFF07FF) | ((a << 11) & 0x0000F800))
// special flag operations
#define MOVE_GET_FLAGS(a) ((a & 0x00000700) >> 8)
#define MOVE_SET_FLAGS(z,a) z = ((z & 0xFFFFF8FF) | ((a << 8) & 0x00000700))
// number of children
#define MOVE_GET_NCHLD(a) (a & 0x000000FF)
#define MOVE_SET_NCHLD(z,a) z = ((z & 0xFFFFFF00) | (a & 0x000000FF))

#define MOVE_JAIL_NONE 31
#define MOVE_FLAG_PROMOTED 0x1
#define MOVE_FLAG_MOVED 0x2

#define MAKE_POS(r,f) ((f*BOARD_DIMENSION)+r)

#define MAX_EVAL 1000



chessBoard::chessBoard()
{
	m_nPiece = BOARD_DIMENSION * BOARD_DIMENSION;
	m_whites = NULL;
	m_blacks = NULL;
	clearBoard();
	m_maxTreeDepth = 4;
	m_curTreeDepth = 0;
	m_baseMoveNum = 0;
	m_baseMoveWhite = true;
	m_nMoves = 0;
	m_fpLog = stdout;
	m_boardInfo.size = BOARD_DIMENSION;
	m_boardInfo.bSelfWhite = false;
	m_boardInfo.bSelfBlack = false;
	m_nLine = 0;
	m_pBestLines = NULL;
	setLineCount(8);
	m_bCheckMate = false;
	m_watchPly = 0;
	m_watchMove = MOVE_INVALID;
}

chessBoard::~chessBoard()
{
	deleteBoard();
	delete m_pBestLines;
}

void chessBoard::deleteBoard()
{
	if(m_whites != NULL) {
		delete[] m_whites;
		m_whites = NULL;
	}
	if(m_blacks != NULL) {
		delete[] m_blacks;
		m_blacks = NULL;
	}
	memset(m_board, 0, sizeof(m_board));
	memset(m_jail, 0, sizeof(m_jail));
}

void chessBoard::clearBoard()
{
	int i;

	for(i = 0; i < BOARD_SIZE; i++)
		m_board[i] = NULL;
	for(i = 0; i < 32; i++)
		m_jail[i] = NULL;
}

void chessBoard::resetBoard()
{
	int i, j, k;

	deleteBoard();

	m_whites = new chessPiece[NPIECE];
	m_blacks = new chessPiece[NPIECE];
	for(i = 0; i < NPIECE; i++) {
		m_whites[i].setBoard(&m_boardInfo);
		m_blacks[i].setBoard(&m_boardInfo);
	}

	i = 0;
	m_whites[i].init(i, ID_WHITE_ROOK);    m_board[i] = &m_whites[i]; i++;
	m_whites[i].init(i, ID_WHITE_KNIGHT);  m_board[i] = &m_whites[i]; i++;
	m_whites[i].init(i, ID_WHITE_BISHOP);  m_board[i] = &m_whites[i]; i++;
	m_whites[i].init(i, ID_WHITE_QUEEN);   m_board[i] = &m_whites[i]; i++;
	m_whites[i].init(i, ID_WHITE_KING);    m_board[i] = &m_whites[i]; i++;
	m_whites[i].init(i, ID_WHITE_BISHOP);  m_board[i] = &m_whites[i]; i++;
	m_whites[i].init(i, ID_WHITE_KNIGHT);  m_board[i] = &m_whites[i]; i++;
	m_whites[i].init(i, ID_WHITE_ROOK);    m_board[i] = &m_whites[i]; i++;

	i = 0;
	k = BOARD_SIZE - 1;
	m_blacks[i].init(k-i, ID_BLACK_ROOK);    m_board[k-i] = &m_blacks[i]; i++;
	m_blacks[i].init(k-i, ID_BLACK_KNIGHT);  m_board[k-i] = &m_blacks[i]; i++;
	m_blacks[i].init(k-i, ID_BLACK_BISHOP);  m_board[k-i] = &m_blacks[i]; i++;
	m_blacks[i].init(k-i, ID_BLACK_KING);   m_board[k-i] = &m_blacks[i]; i++;
	m_blacks[i].init(k-i, ID_BLACK_QUEEN);    m_board[k-i] = &m_blacks[i]; i++;
	m_blacks[i].init(k-i, ID_BLACK_BISHOP);  m_board[k-i] = &m_blacks[i]; i++;
	m_blacks[i].init(k-i, ID_BLACK_KNIGHT);  m_board[k-i] = &m_blacks[i]; i++;
	m_blacks[i].init(k-i, ID_BLACK_ROOK);    m_board[k-i] = &m_blacks[i]; i++;
	//
	// do pawns
	j = BOARD_DIMENSION;
	k = BOARD_DIMENSION * (BOARD_DIMENSION-1) - 1;
	for(i = 0; i < BOARD_DIMENSION; i++) {
		m_whites[i+j].init(i+j, ID_WHITE_PAWN);
		m_board[i+j] = &m_whites[i+j];

		m_blacks[i+j].init(k-i, ID_BLACK_PAWN);
		m_board[k-i] = &m_blacks[i+j];
	}
}


int chessBoard::printBoard(FILE *f)
{
	int rank,file;
	int i;

	if(f == NULL)
		return 1;

	fprintf(f, "   a b c d e f g h\n");
	fprintf(f, "  #-|-|-|-|-|-|-|-#\n");
	for(file = BOARD_DIMENSION-1; file >= 0; file--)
	{
		fprintf(f, "%d |", file+1);
		for(rank = 0; rank < BOARD_DIMENSION; rank++)
		{
			i = (file*BOARD_DIMENSION)+rank;
			if(m_board[i] == NULL)
			{
				if(rank == BOARD_DIMENSION-1)
					fprintf(f, "%c", '.');
				else
					fprintf(f, "%c ", '.');
				continue;
			}
			if(rank == BOARD_DIMENSION-1)
				fprintf(f, "%c", m_board[i]->getSym());
			else
				fprintf(f, "%c ", m_board[i]->getSym());
		}
		fprintf(f, "| %d\n", file+1);
	}
	fprintf(f, "  #-|-|-|-|-|-|-|-#\n");
	fprintf(f, "   a b c d e f g h\n");
	fprintf(f, "\n");
	return 1;
}

int chessBoard::setLogFile(FILE *fp)
{
	m_fpLog = fp;
	return 1;
}

void chessBoard::setMaxTreeDepth(unsigned depth)
{
	if(depth == m_maxTreeDepth || depth >= MAX_TREE_DEPTH)
		return;
	m_maxTreeDepth = depth;
}

unsigned chessBoard::getMaxTreeDepth()
{
	return m_maxTreeDepth;
}

int chessBoard::setLineCount(unsigned nLine)
{
	//unsigned i;
	if(nLine == m_nLine)
		return 1;

	m_nLine = nLine;

	clearLines();
	if(m_pBestLines != NULL)
		delete m_pBestLines;
	m_pBestLines = new LossyPriorityQueue<moveLine>(m_nLine);
	return 1;
}

void chessBoard::clearLines()
{
	if(m_pBestLines == NULL)
		return;
	while(!m_pBestLines->empty())
		m_pBestLines->pop();
}

void chessBoard::setSelfWhite(bool b)
{
	m_boardInfo.bSelfWhite = b;
}

void chessBoard::setSelfBlack(bool b)
{
	m_boardInfo.bSelfBlack = b;
}

int chessBoard::str2move(unsigned int *move, char *strMove)
{
	int pos1, pos2;
	int p1file, p1rank;
	int p2file, p2rank;
	char c;

	if(strMove == NULL)
		return MOVE_ERROR;

	if(strlen(strMove) < 4)
		return MOVE_ERROR;

	c = strMove[0];
	if(!isalpha(c))
		return MOVE_ERROR;
	c = tolower(c);
	if(c < 'a' || c > 'h') // 97 == 'a',  104 == 'h'
		return MOVE_ERROR;
	p1rank = c - (int)'a';
	c = strMove[1];
	if(!isdigit(c))
		return MOVE_ERROR;
	if(c < '1' || c > '8') // 49 == '1', 56 == '8'
		return MOVE_ERROR;
	p1file = c - (int)'1';
	c = strMove[2];
	if(!isalpha(c))
		return MOVE_ERROR;
	c = tolower(c);
	if(c < 'a' || c > 'h')
		return MOVE_ERROR;
	p2rank = c - (int)'a';
	c = strMove[3];
	if(!isdigit(c))
		return MOVE_ERROR;
	if(c < '1' || c > '8')
		return MOVE_ERROR;
	p2file = c - (int)'1';

	pos1 = MAKE_POS(p1rank, p1file);
	pos2 = MAKE_POS(p2rank, p2file);
	*move = 0;
	MOVE_SET_POS(*move, pos1, pos2);
	return 1;
}

int chessBoard::forwardMove(bool bWhite, char *pos)
{
	int status;
	unsigned int move;
	bool bInCheck;

	if((status = str2move(&move, pos)) <= MOVE_ERROR)
		return status;

	m_baseMoveWhite = m_baseMoveNum % 2 == 0;
	if(m_baseMoveWhite)
		setSelfWhite(false);
	else
		setSelfBlack(false);

	return forwardMove(move, bInCheck);
}

int chessBoard::forwardMove()
{
	unsigned j;
	unsigned int move;
	bool bInCheck = false;

	srand(time(NULL)); // seed random generator for undecided moves

	m_bCheckMate = false;
	m_curTreeDepth = 0;
	m_nMoves = 0;

	//
	// let the move creation code know that we are the computer.
	// It will do extra checks like avoiding piece shuffling.
	m_baseMoveWhite = m_baseMoveNum % 2 == 0;
	if(m_baseMoveWhite)
		setSelfWhite(true);
	else
		setSelfBlack(true);

	clearLines();
	findBestLines(heuristic(), bInCheck); // recursive minimax search 
	move = m_pBestLines->top().moves[0];

	//
	// make the move if we aren't in checkmate
	if(!m_bCheckMate)
	{
		moveLine &line = m_pBestLines->top();
		//
		// use first move of best line
		//move = m_bestLines[0]->moves[0];
		forwardMove(line.moves[0], bInCheck);
		setLastMove(line.moves[0]);
	}

	//
	// print out statistics and thinking information about the
	// move just made.
	//
	if(m_fpLog != NULL)
	{
		fprintf(m_fpLog, "%d moves considered\n", m_nMoves);
		while(!m_pBestLines->empty())
		{
			moveLine line = m_pBestLines->top();
			m_pBestLines->pop();

			fprintf(m_fpLog, "[%.4f,%.4f ] ", line.predict(), line.evaluate());
			//fprintf(m_fpLog, "[%d] ", line.prediction);
			for(j = 0; j < m_maxTreeDepth; j++) {
				fprintf(m_fpLog, "%d ", line.evals[j]);
				fprintf(m_fpLog, "%-5s ", move2str(line.moves[j]));
			}
			fprintf(m_fpLog, "\n");
			fflush(m_fpLog);
		}
	}
	return 1;
}

int chessBoard::setBaseMove(int nMove)
{
	m_baseMoveNum = nMove;
	return TRUE;
}

bool chessBoard::inCheck(bool bWhite)
{
	int j;
	int error;
	chessPiece *piece;
	chessPiece *lastPiece;
	int piecePos;
	unsigned int child;
	int nMoves;
	int newMoves[MAX_PIECE_MOVES];
	bool bInCheck = false;

	if(bWhite) {
		piece = m_blacks;
		lastPiece = &m_blacks[NPIECE-1];
	} else {
		piece = m_whites;
		lastPiece = &m_whites[NPIECE-1];
	}

	for(; piece <= lastPiece; piece++)
	{
		if(!piece->alive())
			continue;

		piecePos = piece->getPos();
		nMoves = piece->getPossibleMoves(m_board, newMoves);
		for(j = 0; j < nMoves; j++)
		{
			child = 0;
			MOVE_SET_POS(child, piecePos, newMoves[j]);
			MOVE_SET_NCHLD(child, 0);
			if(forwardMove(child, bInCheck) < 0)
				continue;

			error = reverseMove(child);
			if(error < MOVE_ERROR) {
				if(m_fpLog != NULL)
					fprintf(m_fpLog, "reverseMove has a booboo\n");
				printError(error);
			}

			if(bInCheck)
				return true; // at least one king capture
		}
	}
	return false;
}

int chessBoard::watchMove(int ply, char *strMove)
{
	int status;
	unsigned int move;

	if(ply < 0 || ply >= (int)m_maxTreeDepth)
		return MOVE_ERROR;
	m_watchPly = (unsigned int)ply;

	if((status = str2move(&move, strMove)) < MOVE_ERROR)
		return status;
	m_watchMove = move;
	return 1;
}

/********************************************
 *
 * protected memboer methods
 *
 */


int chessBoard::forwardMove(unsigned int &move, bool &seenCheck)
{
	int i;
	int hid; // hostage id
	int src = MOVE_POS_SRC(move);
	int dst = MOVE_POS_DST(move);
	int srcPos = PIECE_POS(src);
	int dstPos = PIECE_POS(dst);
	int moves[MAX_PIECE_MOVES];
	int nMoves;
	chessPiece *srcPiece = m_board[srcPos];
	chessPiece *dstPiece = m_board[dstPos];

	if(srcPiece == NULL) {
		if(m_fpLog != NULL)
			fprintf(m_fpLog,"forwardMove - move error src:%d invalid\n",srcPos);
		return MOVE_ERROR_SRC_INVALID;
	}
	nMoves = srcPiece->getPossibleMoves(m_board, moves);
	if(nMoves <= 0) {
		if(m_fpLog != NULL)
			fprintf(m_fpLog,"forwardMove - dst possible moves: %d\n", nMoves);
		return MOVE_ERROR_DST_INVALID;
	}
	for(i = 0; i < nMoves; i++) {
		// find out if this move is valid
		if(dstPos == PIECE_POS(moves[i]))
			break;
	}
	if(i >= nMoves) {
		if(m_fpLog != NULL)
			fprintf(m_fpLog,"forwardMove - move error dst invalid\n");
		return MOVE_ERROR_DST_INVALID;
	}
	m_gameMove++; // advance move counter for the game by one

	dst = moves[i]; // get castling flag back if necessary
	//
	// we should have a valid move now, so start the move
	if(dstPiece != NULL)
	{
		for(i = 0; i < 32; i++) {
			if(m_jail[i] == NULL)
				break;
		}
		ASSERT(i < 32);
		MOVE_SET_JAIL(move, i); // offset in jail so we can find later
		hid = dstPiece->getId();
		if(hid == ID_BLACK_KING || hid == ID_WHITE_KING)
			seenCheck = true; // opponent is in check on higher level
		m_jail[i] = dstPiece; // put piece in jail
		m_board[dstPos]->alive(false);
		m_board[dstPos] = NULL;
	} else {
		MOVE_SET_JAIL(move, MOVE_JAIL_NONE); // no hostage
	}
	srcPiece->setPosForward(dstPos, m_gameMove);
	m_board[dstPos] = srcPiece;
	m_board[srcPos] = NULL;
	dstPiece = m_board[dstPos];
	//
	// do any pawn promotions
	if(srcPiece->getId() == ID_WHITE_PAWN &&
		dstPos >= (BOARD_DIMENSION-1)*(BOARD_DIMENSION))
	{
		dstPiece->setId(ID_WHITE_QUEEN);
		MOVE_SET_FLAGS(move, MOVE_FLAG_PROMOTED);
	}
	if(srcPiece->getId() == ID_BLACK_PAWN && dstPos < BOARD_DIMENSION)
	{
		dstPiece->setId(ID_BLACK_QUEEN);
		MOVE_SET_FLAGS(move, MOVE_FLAG_PROMOTED);
	}
	// castling code hacked for (BOARD_DIMENSION == 8)
	if(dst & PIECE_FLAG_CASTLING)
	{
		if(dstPiece->isWhite())
		{
			if(dstPos == 6) {
				m_board[5] = m_board[7];
				m_board[7] = NULL;
				m_board[5]->setPosForward(5, m_gameMove);
			}
			if(dstPos == 2) {
				m_board[3] = m_board[0];
				m_board[0] = NULL;
				m_board[3]->setPosForward(3, m_gameMove);
			}
		}
		else
		{
			if(dstPos == 58) {
				m_board[59] = m_board[56];
				m_board[56] = NULL;
				m_board[59]->setPosForward(59, m_gameMove);
			}
			if(dstPos == 62) {
				m_board[61] = m_board[63];
				m_board[63] = NULL;
				m_board[61]->setPosForward(61, m_gameMove);
			}
		}
	}
	if(dst & PIECE_FLAG_EN_PASSANT)
	{
		int idx;

		if(dstPiece->isWhite())
			idx = dstPos - BOARD_DIMENSION;
		else
			idx = dstPos + BOARD_DIMENSION;

		ASSERT(m_board[idx] != NULL);

		for(i = 0; i < 32; i++) {
			if(m_jail[i] == NULL)
				break;
		}
		ASSERT(i < 32);
		MOVE_SET_JAIL(move, i); // offset in jail so we can find later
		m_jail[i] = m_board[idx]; // put piece in jail
		m_board[idx]->alive(false);
		m_board[idx] = NULL;
	}
	return 1;
}

int chessBoard::reverseMove(unsigned int &move)
{
	int src = MOVE_POS_SRC(move);
	int dst = MOVE_POS_DST(move);
	int srcPos = PIECE_POS(src);
	int dstPos = PIECE_POS(dst);
	int jail_id;

	if(m_board[dstPos] == NULL) {
		if(m_fpLog != NULL) {
			fprintf(m_fpLog,"reverseMove - move error dst invalid\n");
			ASSERT(0);
		}
		return MOVE_ERROR_DST_INVALID;
	}
	m_board[srcPos] = m_board[dstPos];
	m_board[srcPos]->setPosReverse(srcPos);
	m_board[dstPos] = NULL;
	//
	// reverse pawn promotion
	if(MOVE_GET_FLAGS(move) & MOVE_FLAG_PROMOTED) {
		if(m_board[srcPos]->isWhite())
			m_board[srcPos]->setId(ID_WHITE_PAWN);
		else
			m_board[srcPos]->setId(ID_BLACK_PAWN);
	}
	// reverse castling
	if(dst & PIECE_FLAG_CASTLING)
	{
		if(m_board[srcPos]->isWhite()) {
			if(dstPos == 6) {
				m_board[7] = m_board[5];
				m_board[5] = NULL;
				m_board[7]->setPosReverse(7);
			}
			if(dstPos == 2) {
				m_board[0] = m_board[3];
				m_board[3] = NULL;
				m_board[0]->setPosReverse(0);
			}
		} else {
			if(dstPos == 58) {
				m_board[56] = m_board[59];
				m_board[59] = NULL;
				m_board[56]->setPosReverse(56);
			}
			if(dstPos == 62) {
				m_board[63] = m_board[61];
				m_board[61] = NULL;
				m_board[63]->setPosReverse(63);
			}
		}
	}
	if(dst & PIECE_FLAG_EN_PASSANT)
	{
		int idx;

		if(m_board[srcPos]->isWhite())
			idx = dstPos - BOARD_DIMENSION;
		else
			idx = dstPos + BOARD_DIMENSION;

		ASSERT(m_board[idx] == NULL);

		jail_id = MOVE_GET_JAIL(move);
		ASSERT(jail_id != MOVE_JAIL_NONE);

		m_board[idx] = m_jail[jail_id];
		m_board[idx]->alive(true);
		m_jail[jail_id] = NULL;

		MOVE_SET_JAIL(move, MOVE_JAIL_NONE); // abinormal hostage
	}
	// reverse piece capture
	jail_id = MOVE_GET_JAIL(move);
	if(jail_id != MOVE_JAIL_NONE) {
		ASSERT(m_jail[jail_id] != NULL);
		m_board[dstPos] = m_jail[jail_id];
		m_board[dstPos]->alive(true);
		m_jail[jail_id] = NULL;
	}

	m_gameMove--; // back the board move counter up by one
	return 1;
}

int chessBoard::heuristic()
{
	bool whitesMove = ((m_baseMoveNum + m_curTreeDepth) % 2) == 0;
	/*
	int i;
	int whiteRank = 0;
	int blackRank = 0;

	for(i = 0; i < 32; i++)
	{
		if(m_jail[i] == NULL)
			continue;
		if(m_jail[i]->isWhite())
		{
			//whiteRank -= m_jail[i]->getRank();
			blackRank += m_jail[i]->getRank() * 10;
		}
		else
		{
			whiteRank += m_jail[i]->getRank() * 10;
			//blackRank -= m_jail[i]->getRank();
		}
	}

	if(m_baseMoveWhite)
		return whiteRank - blackRank; // MAXimize white
	else
		return blackRank - whiteRank; // MAXimize black
	*/
	/*
	 * relative pieces on board... sucks
	 */
	int whiteRank;
	int blackRank;
	chessPiece *piece;
	//bool whitesMove = ((m_baseMoveNum + m_curTreeDepth) % 2) == 0;

	whiteRank = 0;
	for(piece = m_whites; piece <= &m_whites[NPIECE-1]; piece++)
		if(piece->alive()) {
	//		if(m_baseMoveNum < 10)
	//			whiteRank += piece->m_selfRank;
	//		else
				whiteRank += piece->getRank() * 100;
		}

	blackRank = 0;
	for(piece = m_blacks; piece <= &m_blacks[NPIECE-1]; piece++)
		if(piece->alive()) {
	//		if(m_baseMoveNum < 10)
	//			blackRank += piece->m_selfRank;
	//		else
				blackRank += piece->getRank() * 100;
		}

	//if(whitesMove)
	if(m_baseMoveWhite)
		return whiteRank - blackRank; // MAXimize white
	else
		return blackRank - whiteRank; // MAXimize black
	/*
	 * radom 
	return 1+(int)(20.0 * rand()/(RAND_MAX+1.0));
	 */
}

int chessBoard::findBestLines(int parentRank, bool &parentInCheck)
{
	//unsigned i;
	int j;
	int error;
	//int rank;
	//int dRank;
	int curRank;
	int bestRank;
	int nChild = 0;
	int nValidChild = 0;
	unsigned int child[MAX_PIECE_MOVES*16]; // 2kb move buffer!
	//unsigned bestMoves[MAX_TREE_DEPTH];
	//int bestRanks[MAX_TREE_DEPTH];
	int maxRank;
	//int bestMove = 0;
	int nMoves;
	int newMoves[MAX_PIECE_MOVES];
	int piecePos;
	chessPiece *piece;
	chessPiece *lastPiece;
	bool bInCheck;

	bool whitesMove = ((m_baseMoveNum + m_curTreeDepth) % 2) == 0;
	if(whitesMove) {
		piece = m_whites;
		lastPiece = &m_whites[NPIECE-1];
	} else {
		piece = m_blacks;
		lastPiece = &m_blacks[NPIECE-1];
	}
	bool bMaximize = m_baseMoveWhite == whitesMove;
	if(bMaximize)
		maxRank = -2147483640;
	else
		maxRank = 2147483640;
	bestRank = maxRank;

	for(; piece <= lastPiece; piece++)
	{
		if(!piece->alive())
			continue;

		piecePos = piece->getPos();
		nMoves = piece->getPossibleMoves(m_board, newMoves);

		m_nMoves += nMoves;

		for(j = 0; j < nMoves; j++)
		{

			child[nChild] = 0;
			MOVE_SET_POS(child[nChild], piecePos, newMoves[j]);
			MOVE_SET_NCHLD(child[nChild], 0);
			if(forwardMove(child[nChild], parentInCheck) < 0)
				continue;

			curRank = heuristic();

			//
			// diminish rank slightly as predictions go further away
			// from move zero.  only do it in move increments of two
			// so as to not mess with immediate sacrifices.
			//
			if(bMaximize)
				curRank -= m_curTreeDepth / 2;
			else
				curRank += m_curTreeDepth / 2;

			//rank = dRank = curRank - parentRank; // differential heuristic

			//
			// append move to current move line
			m_curLine.moves[m_curTreeDepth] = child[nChild];
			m_curLine.evals[m_curTreeDepth] = curRank;


			if(m_curTreeDepth < m_maxTreeDepth-1)
			{
				bInCheck = false; // flag set via recursion
				m_curTreeDepth++;
				findBestLines(curRank, bInCheck);
				m_curTreeDepth--;

			}
			else
				bInCheck = false;

			error = reverseMove(child[nChild]);
			if(error < MOVE_ERROR) {
				if(m_fpLog != NULL)
					fprintf(m_fpLog, "reverseMove has a booboo\n");
				printError(error);
			}

			if(bInCheck) {
				nChild++; // we can't move into check
				continue;
			}

			if(m_curTreeDepth == m_maxTreeDepth-1)
			{
				// this is a leaf node, submit it's line to be
				// considered as best in the tree
				updateBestLine(curRank);
			}

			nChild++;
			nValidChild++;
		}
	}

	if(!nValidChild)
	{
		if(m_curTreeDepth == 0)
			m_bCheckMate = true;
	}
	return nValidChild;
}

char* chessBoard::move2str(unsigned int move, bool bExtra)
{
	int i;
	int file, rank, dstFile, dstRank;
	static char str[16];

	i = PIECE_POS(MOVE_POS_SRC(move));
	file = i / BOARD_DIMENSION;
	rank = 'a' + (i - (file * BOARD_DIMENSION)) - 1;

	i = PIECE_POS(MOVE_POS_DST(move));
	dstFile = i / BOARD_DIMENSION;
	dstRank = 'a' + (i - (dstFile * BOARD_DIMENSION)) - 1;

	if((int)MOVE_GET_FLAGS(move) & MOVE_FLAG_PROMOTED)
	{
		snprintf(str, sizeof(str), "%c%d%c%dq", 
				rank+1, file+1, dstRank+1, dstFile+1);
	}
	else {
		snprintf(str, sizeof(str), "%c%d%c%d", 
				rank+1, file+1, dstRank+1, dstFile+1);
	}
	if(bExtra)
	{
		if(MOVE_GET_JAIL(move) != MOVE_JAIL_NONE)
			strcat(str, "*");
	}
	return str;
}

int chessBoard::setLastMove(unsigned int move)
{
	strncpy(m_strLastMove, move2str(move, false), sizeof(m_strLastMove));
	return 1;
}

void chessBoard::printError(int error)
{
	switch(error)
	{
		case MOVE_ERROR_OUT_OF_BOUNDS:
			if(m_fpLog != NULL)
				fprintf(m_fpLog,"ERROR: tried to move out of bounds\n");
			break;
		case MOVE_ERROR_SRC_INVALID:
			if(m_fpLog != NULL)
				fprintf(m_fpLog,"ERROR: invalid src piece\n");
			break;
		case MOVE_ERROR_DST_INVALID:
			if(m_fpLog != NULL)
				fprintf(m_fpLog,"ERROR: invalid dst piece\n");
			break;
		default:
			if(m_fpLog != NULL)
				fprintf(m_fpLog,"ERROR: unknown\n");
	};
}


int chessBoard::dumpMove(unsigned int move)
{
	if(m_fpLog == NULL)
		return 0;
	fprintf(m_fpLog, "## move dump: src:%d dst:%d jail:%d nch:%d flg:%d",
			MOVE_POS_SRC(move),
			MOVE_POS_DST(move),
			MOVE_GET_JAIL(move),
			MOVE_GET_NCHLD(move),
			MOVE_GET_FLAGS(move));
	return 1;
}

void chessBoard::updateBestLine(int curEval)
{
	int i;

	if(m_watchMove != MOVE_INVALID)
	{
	   if(MOVE_POS_SRC(m_curLine.moves[m_watchPly]) ==
		  MOVE_POS_SRC(m_watchMove))
	   {
		   if(MOVE_POS_DST(m_curLine.moves[m_watchPly]) ==
		      MOVE_POS_DST(m_watchMove))
		//return; // ignore line if we are watching for specific line
			i = 42;
	   }
	}

	//m_curLine.evaluation = curEval;

	int nPly = m_maxTreeDepth - (m_maxTreeDepth % 2);
	m_curLine.prediction = 0;
	m_curLine.evaluation = 0;

	for(i = 0; i < nPly; i++)
	{
		m_curLine.prediction += m_curLine.evals[i];

		m_curLine.evaluation += m_curLine.evals[i] * (nPly-i) * MAX_EVAL;
		//if(m_baseMoveWhite && i % 2 == 0)
		//	m_curLine.evaluation += m_curLine.evals[i];
		//else
		//	m_curLine.evaluation += (0 - m_curLine.evals[i]);
	}
	m_pBestLines->push(m_curLine);
}


