
/*
 * Dendron, a lousy chess engine
 * Copyright 2005, James D. Taylor
 *
 * main.cpp
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#ifndef WIN32
  #include <signal.h>
#endif
#include "chessBoard.h"

#define CHESS_TITLE "dendron v0.42"

#define MAX_CMD_LEN 128

#ifdef WIN32
  #define sleep _sleep
#endif

#define SLEEP sleep

static FILE *logFile = NULL;
chessBoard cBoard;

int PUTS(const char *s)
{
	char buf[2048];

	puts(s);

	strcpy(buf, "<-- ");
	strncat(buf, s, sizeof(buf));
	if(logFile != NULL) {
		fprintf(logFile, buf);
		fflush(logFile);
	}
	fflush(stdout);
	return 1;
}

int PRINTF(const char *format, ...)
{
	char buf[2048];
	va_list vl;

	va_start(vl, format);
	strcpy(buf, "--> ");
	strncat(buf, format, sizeof(buf));
	vprintf(format, vl);
	if(logFile != NULL)
		vfprintf(logFile, buf, vl);
	fflush(stdout);
	va_end(vl);
	return 1;
}


int printHelp(char *progName)
{
	printf("\n");
	printf("Dendron; a lousy chess engine.\n");
	printf("Copyright 2005 James D. Taylor\n");
	printf("\n");
	printf("%s [-liadh]\n", progName);
	printf("-i         - interactive mode (console)\n");
	printf("-l         - log incoming protocol to log.txt\n");
	printf("-a         - have it automatically play itself\n");
	printf("-d <depth> - set the engine to search to <depth> ply\n");
	printf("-h         - this help screen\n");
	printf("\n");
	return 1;
}

void sig_int(int hmm)
{
	if(logFile != NULL)
		fprintf(logFile, "## received SIGINT.  ignoring.\n");
}

int ignore_signals()
{
#ifndef WIN32
	struct sigaction sa;
	memset(&sa, 0, sizeof(sa));
	sa.sa_handler = SIG_IGN;
	//sa.sa_handler = sig_int;
	sigaction(SIGINT, &sa, NULL);
#endif
	return 1;
}

int do_checkMate(bool bWhite)
{
	char errBuf[MAX_CMD_LEN];
	if(bWhite) // white has been check-mated
		snprintf(errBuf, sizeof(errBuf), "0-1 {Black mates}\n");
	else
		snprintf(errBuf, sizeof(errBuf), "1-0 {White mates}\n");

	PUTS(errBuf);
	if(logFile != NULL) {
		cBoard.printBoard(logFile);
		fflush(logFile);
	}
	return 1;
}

int do_illegalMove(bool bWhite, char *move)
{
	char errBuf[MAX_CMD_LEN];
	snprintf(errBuf, sizeof(errBuf), "Illegal move: %s\n", move);
	PUTS(errBuf);
	if(logFile != NULL) {
		cBoard.printBoard(logFile);
		fflush(logFile);
	}
	return 1;
}

int main(int argc, char *argv[])
{
	int i, j;
	char cmdBuf[MAX_CMD_LEN];
	char inBuf[MAX_CMD_LEN + 2];
	int xboardMode = TRUE;
	int depth = 4; // default depth (ply) for the engine to search
	int bLog = TRUE;
	int bAuto = FALSE;
	bool bWhite = true;
	bool bForce = false;
	int rtrn;
	char strRecordFile[256];
	FILE *fpRecord = NULL;
	FILE *fpCommands = stdin;

	inBuf[0] = '>';
	inBuf[1] = ' ';
	inBuf[2] = '\0';

	for(i = 1; i < argc; i++)
	{
		j = 0;
		if(j < (int)strlen(argv[i]) && argv[i][j] != '-') {
			printHelp(argv[0]);
			return 1;
		}
		for(j = 1; j < (int)strlen(argv[i]); j++)
		{
			if(argv[i][j] == 'h') {
				printHelp(argv[0]);
				return 0;
			}
			else if(argv[i][j] == 'i') {
				j++;
				xboardMode = FALSE;
			}
			else if(argv[i][j] == 'l') {
				j++;
				bLog = TRUE;
			}
			else if(argv[i][j] == 'a') {
				j++;
				bAuto = TRUE;
			}
			else if(argv[i][j] == 'd') {
				if(i+1 >= argc) {
					printHelp(argv[0]);
					return 1;
				}
				depth = atoi(argv[i+1]);
				i++; // skip command line argument
			}
		}
	}
	if(bLog)
		logFile = fopen("log.txt", "w");
	//
	// switch standard i/o buffering to line buffered
	if(setvbuf(stdin, (char*)NULL, _IOLBF, BUFSIZ) != 0) {
		printf("error: couldn't set stdin to line buffered\n");
		return 0;
	}
	if(setvbuf(stdout, (char*)NULL, _IOLBF, BUFSIZ) != 0) {
		printf("error: couldn't set stdout to line buffered\n");
		return 0;
	}

	cBoard.resetBoard();
	cBoard.setMaxTreeDepth(depth);
	cBoard.setLineCount(20);

	if(!xboardMode) {
		cBoard.printBoard(stdout);
		printf("Max Tree Depth set to: %d\n", cBoard.getMaxTreeDepth());
	}

	cmdBuf[0] = '\0';
	bool stop = FALSE;
	i = 0;


	if(bAuto)
	{
		//
		// engine plays iteself in text mode
		//
		cBoard.setSelfWhite(true);
		cBoard.setSelfBlack(true);

		cBoard.setLogFile(logFile); // don't print errors to stdout
		while(42)
		{
			cBoard.setBaseMove(i);
			cBoard.forwardMove();
			PRINTF("move: %d\n", i);
			cBoard.printBoard(stdout);
			cBoard.printBoard(logFile);
			i++;
		}
	}
	if(xboardMode)
	{
		//
		// engine will transmit commands via STDIO using the Chess
		// Engine Protocol.  This is thee method used to communicate
		// with xboard chess GUI.
		//
		if(ignore_signals() < 0)
			return -1;
		cBoard.setLogFile(logFile); // don't print errors to stdout


		while(!stop && fgets(cmdBuf, sizeof(cmdBuf), stdin) != NULL)
		{
			if(logFile != NULL && cmdBuf[0] != '\0') {
				inBuf[0] = '\0';
				strcpy(inBuf, "--> ");
				strncat(inBuf, cmdBuf, sizeof(inBuf));
				fputs(inBuf, logFile);
				fflush(logFile);
			}

			if(strncmp(cmdBuf, "accepted", 8) == 0) {
				// reply from feature command
			}
			if(strncmp(cmdBuf, "black", 5) == 0) {
				// ceiling to an odd move number
				if(i % 2 == 0)
					i++;
				bWhite = false;
				// Set Black on move. Set the engine to play White. 
				cBoard.setSelfBlack(true);
				//cBoard.setSelfWhite(true);
			}
			//
			// case 'f':
			if(strncmp(cmdBuf, "force", 5) == 0) {
				// Set the engine to play neither color ("force
				// mode"). Stop clocks. The engine should check that
				// moves received in force mode are legal and made in
				// the proper turn, but should not think, ponder, or
				// make moves of its own. 
				bForce = true;
				continue;
			}
			//
			// case 'g':
			if(strncmp(cmdBuf, "go", 2) == 0) {
				// Leave force mode and set the engine to play the
				// color that is on move. Associate the engine's clock
				// with the color that is on move, the opponent's
				// clock with the color that is not on move. Start the
				// engine's clock. Start thinking and eventually make
				// a move. 
				if(bForce)
				{
					bForce = false;

					cBoard.setBaseMove(i);
					cBoard.forwardMove();
					if(cBoard.inCheckMate()) {
						do_checkMate(i % 2 == 0);
						bForce = true;
						continue;
					}
					i++;
					snprintf(cmdBuf, sizeof(cmdBuf), "move %s\n",
							cBoard.getLastMove());
					PUTS(cmdBuf);
				}
				continue;
			}
			//
			// case 'n':
			if(strncmp(cmdBuf, "new", 3) == 0) {
				// xboard new game
				cBoard.resetBoard();
				i = 0;
				continue;
			}
			if(strncmp(cmdBuf, "hard", 4) == 0) {
				continue;
			}
			//
			// case 'p':
			if(strncmp(cmdBuf, "protover", 8) == 0) {
				// sent directly after xboard command
				PRINTF("feature usermove=1\n");
				PRINTF("feature myname=\"%s\"\n", CHESS_TITLE);
				PRINTF("feature done=1\n");
				continue;
			}
			if(strncmp(cmdBuf, "quit", 4) == 0) {
				break;
			}
			//
			// case 'r':
			if(strncmp(cmdBuf, "random", 6) == 0) {
				// part of initString command
				continue;
			}
			if(strncmp(cmdBuf, "rejected", 8) == 0) {
				// feature rejected
				continue;
			}
			//
			// case 's':
			if(strncmp(cmdBuf, "sd ", 3) == 0) {
				// sd DEPTH
				// set depth to DEPTH
				cBoard.setMaxTreeDepth(atoi(&cmdBuf[3]));
				continue;
			}
			//
			// case 'u':
			if(strncmp(cmdBuf, "usermove ", 9) == 0)
			{
				rtrn = cBoard.forwardMove(i % 2 == 0 ? 1 : 0, &(cmdBuf[9]));
				i++;
				if(!bForce)
				{
					if(rtrn > MOVE_ERROR)
					{
						cBoard.setBaseMove(i);
						cBoard.forwardMove();
						if(cBoard.inCheckMate()) {
							do_checkMate(i % 2 == 0);
							bForce = true;
							continue;
						}
						i++;
						snprintf(cmdBuf, sizeof(cmdBuf), "move %s\n",
								cBoard.getLastMove());
						PUTS(cmdBuf);
					}
					else
					{
						do_illegalMove(i % 2 == 0, &cmdBuf[9]);
						bForce = true;
					}
				}
				continue;
			}
			//
			// case 'v':
			if(strncmp(cmdBuf, "variant", 7) == 0) {
				// part of initString command
				continue;
			}
			if(strncmp(cmdBuf, "white", 5) == 0) {
				if(i % 2 != 0)
					i++;
				bWhite = true;
				// Set White on move. Set the engine to play Black.
				// Stop clocks.
				//cBoard.setSelfBlack(true);
				cBoard.setSelfWhite(true);
				continue;
			}
			//
			// case 'x':
			if(strncmp(cmdBuf, "xboard", 6) == 0) {
				// xboard init
				xboardMode = 1;
				PRINTF("\n"); // required by xboard protocol
				continue;
			}

			if(strlen(cmdBuf) == 5 && cmdBuf[0] >= 'a' && cmdBuf[0] <= 'h') {
				if(cBoard.forwardMove(i % 2 == 0 ? 1 : 0, cmdBuf) > MOVE_ERROR)
				{
					i++;
					cBoard.setBaseMove(i);
					cBoard.forwardMove();
					if(cBoard.inCheckMate()) {
						do_checkMate(i % 2 == 0);
						bForce = true;
						continue;
					}
					i++;
					snprintf(cmdBuf, sizeof(cmdBuf), "move %s\n",
							cBoard.getLastMove());
					PUTS(cmdBuf);
				}
				else
				{
					do_illegalMove(i % 2 == 0, &cmdBuf[9]);
					bForce = true;
				}
			}

		}
	}
	else
	{
		//
		// Interactive text mode.
		//
		cBoard.setSelfWhite(false);
		cBoard.setSelfBlack(true);

		while(!stop && fgets(cmdBuf, MAX_CMD_LEN, fpCommands) != NULL)
		{
			if(logFile != NULL && cmdBuf[0] != '\0') {
				inBuf[2] = '\0';
				strncat(inBuf, cmdBuf, sizeof(inBuf));
				fputs(inBuf, logFile);
				fflush(logFile);
			}
			if(fpRecord != NULL)
				fputs(cmdBuf, fpRecord);

			switch(tolower(cmdBuf[0]))
			{
				case 'c': 
					cBoard.setBaseMove(i);
					cBoard.forwardMove();
					printf("move: %d\n", i);
					cBoard.printBoard(stdout);
					i++;
					break;
				case 'd':
					cBoard.setMaxTreeDepth(atoi(&cmdBuf[2]));
					printf("Max Tree Depth set to: %d\n",
							cBoard.getMaxTreeDepth());
					break;
				case 'm':
					printf("move: %d\n", i);
					if(cBoard.forwardMove(i % 2 == 0 ? 1 : 0,
								&cmdBuf[2]) > MOVE_ERROR)
						i++;
					cBoard.printBoard(stdout);
					break;
				case 'p':
					printf("move: %d\n", i);
					cBoard.printBoard(stdout);
					break;
				case 'q':
					if(strncmp(cmdBuf, "quit", 4) == 0) // xboard cmd
						stop = TRUE;
					stop = TRUE;
					break;
				case 'r':
					cBoard.deleteBoard();
					cBoard.resetBoard();
					i = 0;
					printf("move: %d\n", i);
					cBoard.printBoard(stdout);
					break;
				case 'w':
					if(cBoard.watchMove(atoi(&cmdBuf[7]), &cmdBuf[2]) <= MOVE_ERROR)
						printf("failed to watch move: %s\n", &cmdBuf[2]);
					else
						printf("watching for lines with move: %4s at ply %d\n",
								&cmdBuf[2], atoi(&cmdBuf[7]));
					break;
				case 'f':
					if(fpRecord != NULL)
						fclose(fpRecord);
					sscanf(cmdBuf, "f %s\n", strRecordFile);
					fpRecord = fopen(strRecordFile, "w");
					if(fpRecord == NULL)
						printf("failed to open file %s\n", strRecordFile);
					else
						printf("Logging all commands to: %s\n", strRecordFile);
					break;
				case 'x':
					sscanf(cmdBuf, "x %s\n", strRecordFile);
					fpCommands = fopen(strRecordFile, "r");
					if(fpCommands == NULL) {
						printf("failed to open file: %s\n", strRecordFile);
						fpCommands = stdin;
					}
					break;
				default:
					break;
			}
			//fflush(stdout);
			if(cmdBuf[0] != 'q')
				printf(">");
		}
	}

	if(logFile != NULL)
		fclose(logFile);

	return 1;
}


