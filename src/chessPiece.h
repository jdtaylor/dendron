
/*
 * Dendron, a lousy chess engine
 * Copyright 2005, James D. Taylor
 *
 * chessPiece.h
 */

#ifndef CHESS_PIECE_H
#define CHESS_PIECE_H

enum {FALSE=0, TRUE};

//
// piece positions should never flow >= BOARD_SIZE, so
// I use the higher 2 bits for flags denoting special moves.
//
#define PIECE_FLAG_CASTLING		0x80
#define PIECE_FLAG_EN_PASSANT	0x40
#define PIECE_FLAG_MASK			0xC0
#define PIECE_POS(p) ((p) & ~PIECE_FLAG_MASK)

#define MAX_POS_TRACK 246

enum {
    ID_NULL = 0,
    ID_WHITE_PAWN,
    ID_WHITE_KNIGHT,
    ID_WHITE_BISHOP,
    ID_WHITE_ROOK,
    ID_WHITE_QUEEN,
    ID_WHITE_KING,
    ID_BLACK_PAWN,
    ID_BLACK_KNIGHT,
    ID_BLACK_BISHOP,
    ID_BLACK_ROOK,
    ID_BLACK_QUEEN,
    ID_BLACK_KING,
    MAX_PIECE
};

typedef struct _chessPieceInfo {
    int id;
    int rank;
    bool white;
    char symbol;
    char descr[20];
} chessPieceInfo;

extern chessPieceInfo g_chessPieceInfo[MAX_PIECE];
extern unsigned int g_chessPieceSum;

typedef struct _boardInfo {
	int size;
	bool bSelfWhite; // whether this program is playing white
	bool bSelfBlack;
} boardInfo;

class chessPiece
{
public:
	chessPiece();
    chessPiece(int pos, int id, boardInfo *board);
	void setBoard(boardInfo *board) { m_pBoard = board; }
	int init(int pos, int id);
    int getPos();
    /*
     * setPosForward:
     *   pos:  single position value representing a position on a
     *         chess board.  Values are combinations of both rank and
     *         file positions where file is increments of m_boardSize.
     *         ie. "a1" = 0.   "h8" = (m_boardSize*7)+7.
	 *   gameMove: move number this move is in the current game.
     *   returns:  TRUE if successfull, FALSE otherwise
     */
    int setPosForward(int pos, int gameMove);
    int setPosReverse(int pos);
    int nMoveGet() { return m_nMoves; }
    bool isWhite();
    char *getCharPos();
    int getId() { return m_id; }
    int setId(int id);
    char getSym() { return g_chessPieceInfo[m_id].symbol; }
    int getRank() { return g_chessPieceInfo[m_id].rank; }
    int getRank(int id);
    /*
     * setCharPos:
     *   pos:  2 characters representing a position of a chess board.
     *         first char representing rank ie "a-h" and the second
     *         char representing file ie "1-8".  bottom left white
     *         rook is "a1".  bottom left black rook is "h8"
     */
    int setCharPos(char *pos);
    /* 
     * getPossibleMoves:
     *   board: array of chessPieces representing current state of
     *          the board
     *   moves: buffer of integers to hold all of the possible valid
     *          moves available to this chess piece using the current
     *          state of the chess board.  Caller should mask with
     *          PIECE_FLAG_MASK before using as a positions.. ie:
     *          board_pos = (moves[i] & ~PIECE_FLAG_MASK)
     *   returns: number of integers stored to moves
     */
    int getPossibleMoves(chessPiece **board, int *moves);

	/*
	 * whether the piece has been captured or not.
	 */
	bool alive() const { return m_alive; } 
	void alive(bool b) { m_alive = b; }

    int m_id;
    int m_boardPos;  // c6 = 5*8+2 = 42   // BINGO!
    int m_nMoves; // number of moves this piece has made
    int m_pos[MAX_POS_TRACK]; // holds this piece's position throughout the game
    int m_moveNum[MAX_POS_TRACK]; // what move of the game was each m_pos made
	int m_selfRank; // summation of the ranks of our own pieces we can capture
	boardInfo *m_pBoard; // shared structure across all chess pieces
protected:
	bool m_alive;
private:
};


#endif

