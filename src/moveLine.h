
#ifndef _MOVE_LINE_H
#define _MOVE_LINE_H

#include <stdlib.h>
#include "chessPiece.h"

#define MAX_TREE_DEPTH 42

class moveLine
{
public:
	int evaluation;
	int prediction;
	unsigned moves[MAX_TREE_DEPTH];
	int evals[MAX_TREE_DEPTH];

	static float g_aggressionFactor;

	inline float predict() const
	{
		return abs(prediction) / g_chessPieceSum;
	}

	inline float aggression() const
	{
		//return 1.0f - (prediction + g_chessPieceSum) / (g_chessPieceSum * 2);
		return prediction;
	}

	inline float evaluate() const
	{
		return evaluation;
		//return prediction;
	//	return (aggression() * g_aggressionFactor) +
	//			((1.0f - predict()) * (1.0f - g_aggressionFactor));
	}

	bool operator<(const moveLine &l) const
	{
	//	float eval1, eval2;
	//	float pred1, pred2;
//		float a = 1.0f;
		// return whether line1 is less than line2
		//
		// combination of good prediction and good evaluation
		//
//		if(this->prediction < l.prediction)
//			return true;
//		if(this->prediction > l.prediction)
//			return false;

//		return abs(this->evaluation) > abs(l.evaluation);

//		eval1 = (this->evaluation) / (1.0f + this->prediction);
//		eval2 = (l.evaluation) / (1.0f + l.prediction);
//		return eval1 > eval2;

		return evaluate() >= l.evaluate();
	}
}; 

#endif


