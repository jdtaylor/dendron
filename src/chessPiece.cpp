
/*
 * Dendron, a lousy chess engine
 * Copyright 2005, James D. Taylor
 *
 * chessPiece.cpp
 */

#include <stdio.h>
#include <stdlib.h>
#include "chessPiece.h"
#ifndef ASSERT
#include <assert.h>
#define ASSERT(c) assert(c)
#endif

#ifdef _WIN32
#define snprintf _snprintf
#endif

chessPieceInfo g_chessPieceInfo[MAX_PIECE] =
{
	{0,   0, TRUE, '.', "Empty"},
	{1,  1, TRUE, 'P', "White Pawn"},
	{2,  3, TRUE, 'N', "White Knight"},
	{3,  3, TRUE, 'B', "White Bishop"},
	{4,  5, TRUE, 'R', "White Rook"},
	{5,  9, TRUE, 'Q', "White Queen"},
	{6, 655, TRUE, 'K', "White King"},
	{7,  1, FALSE, 'p', "Black Pawn"},
	{8,  3, FALSE, 'n', "Black Knight"},
	{9,  3, FALSE, 'b', "Black Bishop"},
	{10,  5, FALSE, 'r', "Black Rook"},
	{11,  9, FALSE, 'q', "Black Queen"},
	{12, 655, FALSE, 'k', "Black King"}
};

unsigned int g_chessPieceSum = 
	g_chessPieceInfo[ID_WHITE_PAWN].rank	* 8 +
	g_chessPieceInfo[ID_WHITE_KNIGHT].rank	* 2 +
	g_chessPieceInfo[ID_WHITE_BISHOP].rank	* 2 +
	g_chessPieceInfo[ID_WHITE_ROOK].rank	* 2 +
	g_chessPieceInfo[ID_WHITE_QUEEN].rank +
	g_chessPieceInfo[ID_WHITE_KING].rank;

extern "C" {

typedef int (*fnMoves_t)(chessPiece *self, chessPiece **board, int *moves);

static int dontEmbarrassMyself(chessPiece *self, int *moves, int nMoves);

static int movesPawn(chessPiece *self, chessPiece **board, int *moves);
static int movesKnight(chessPiece *self, chessPiece **board, int *moves);
static int movesBishop(chessPiece *self, chessPiece **board, int *moves);
static int movesRook(chessPiece *self, chessPiece **board, int *moves);
static int movesQueen(chessPiece *self, chessPiece **board, int *moves);
static int movesKing(chessPiece *self, chessPiece **board, int *moves);

};

//
// array of functions that handle listing a particular
// chess-piece's possible moves.
//
static fnMoves_t g_fnMoves[MAX_PIECE] = 
{
	NULL,
	movesPawn,
	movesKnight,
	movesBishop,
	movesRook,
	movesQueen,
	movesKing,
	movesPawn,
	movesKnight,
	movesBishop,
	movesRook,
	movesQueen,
	movesKing
};

chessPiece::chessPiece()
{
	m_boardPos = -1;
	m_id = 0;
	m_nMoves = 0;
	m_alive = false;
	m_selfRank = 0;
	m_pBoard = NULL;
}

chessPiece::chessPiece(int pos, int id, boardInfo *board)
{
	m_boardPos = -1;
	m_id = 0;
	m_nMoves = 0;
	m_selfRank = 0;
	m_pBoard = board;
	init(pos, id);
}

int chessPiece::init(int pos, int id)
{
	if(pos < -1 || pos >= m_pBoard->size*m_pBoard->size)
		return 0;
	if(id < 0 || id >= MAX_PIECE)
		return 0;

	m_boardPos = pos;
	m_id = id;
	m_pos[0] = pos;
	m_moveNum[0] = 0; // initial game position.. game hasn't started yet
	m_selfRank = 0;
	m_alive = true;
	return 1;
}

int chessPiece::getPos()
{
	return m_boardPos;
}

int chessPiece::setPosForward(int pos, int gameMove)
{
	if(pos < -1 || pos >= m_pBoard->size*m_pBoard->size)
		return FALSE;
	ASSERT(m_nMoves < MAX_POS_TRACK-1);

	m_nMoves++;
	m_pos[m_nMoves] = pos; // pos[0] is initial piece position
	m_moveNum[m_nMoves] = gameMove;
	m_boardPos = pos;
	return TRUE;
}

int chessPiece::setPosReverse(int pos)
{
	if(pos < -1 || pos >= m_pBoard->size*m_pBoard->size)
		return FALSE;
	ASSERT(m_nMoves > 0);
	m_nMoves--;
	ASSERT(pos == m_pos[m_nMoves]);
	m_boardPos = pos;
	//    m_boardPos = m_pos[m_nMoves];
	return TRUE;
}

bool chessPiece::isWhite()
{
	return g_chessPieceInfo[m_id].white;
}

char *chessPiece::getCharPos()
{
	int file;
	char rank;
	char *location = NULL;

	if(m_pBoard->size <= 0)
		return NULL;

	location = (char *)malloc(2);
	if(location == NULL)
		return NULL;

	file = m_boardPos / m_pBoard->size;
	rank = 20 + (m_boardPos - (file * m_pBoard->size));

	snprintf(location,2, "%c%d", rank+1, file+1);
	return location;
}

int chessPiece::setCharPos(char *pos)
{
	char rank;
	int file;

	if(pos == NULL)
		return FALSE;

	sscanf(pos, "%c%d", &rank, &file);

	rank -= 21;
	file -= 1;
	if(rank < 0 || rank >= m_pBoard->size)
		return FALSE;
	if(file < 0 || file >= m_pBoard->size)
		return FALSE;

	m_boardPos = (file*m_pBoard->size)+rank;
	m_pos[++m_nMoves] = m_boardPos;

	return m_boardPos;
}

int chessPiece::setId(int id)
{
	if(id < 0 || id >= MAX_PIECE)
		return 0;
	m_id = id;
	return 1;
}

int chessPiece::getRank(int id)
{
	if(id < 0 || id >= MAX_PIECE)
		return 0;

	return g_chessPieceInfo[id].rank;
}

int chessPiece::getPossibleMoves(chessPiece **board, int *moves)
{
	if(m_id <= 0 || m_id >= MAX_PIECE)
		return -1;
	this->m_selfRank = 0;
	return g_fnMoves[m_id](this, board, moves);
}

int dontEmbarrassMyself(chessPiece *self, int *moves, int nMoves)
{
	/*
	 * Well this was a difficult bug.  This was activated for the
	 * human player as well.  If you were to move any piece back to a
	 * position it had already been at, it would crash.. if you do put
	 * it back, then limit it to the AI only.

	if(self->m_pBoard->bSelfWhite && self->isWhite() ||
	   self->m_pBoard->bSelfBlack && !self->isWhite())
	{
		int i, j;
		if(self->m_nMoves >= 1)
		{
			for(i = 0; i < nMoves; i++)
			{
				//	    if(moves[i] == self->m_pos[self->m_nMoves-1] &&
				//		self->m_boardPos == self->m_pos[self->m_nMoves-2])
				if(moves[i] == self->m_pos[self->m_nMoves-1])
				{
					// delete this position to avoid embarassing move. hehe
					for(j = i; j < nMoves-1; j++)
						moves[j] = moves[j+1];
					nMoves--;
				}
			}
		}
	}
	 */
	return nMoves;
}

int movesQueen(chessPiece *self, chessPiece **board, int *moves)
{
	if(moves == NULL)
		return 0;
	if(self->m_boardPos < 0)
		return 0;

	int boardSize = self->m_pBoard->size;
	bool alive1, alive2, alive3;
	alive1 = alive2 = alive3 = TRUE;
	int i;
	int x = 0;
	int idx;
	int file = self->m_boardPos / boardSize;
	int rank = self->m_boardPos - (file * boardSize);
	int diagMax = file;
	int diagMin = file;

	for(i = rank+1; i < boardSize; i++)
	{
		if(alive1)
		{
			idx = file*boardSize+i;
			if(board[idx] != NULL)
			{
				alive1 = FALSE;
				if(board[idx]->isWhite() != self->isWhite())
					moves[x++] = idx;
				else
					self->m_selfRank += board[idx]->getRank();
			}
			else
				moves[x++] = idx;
		}
		if(alive2)
		{
			diagMax++;
			if(diagMax < boardSize)
			{
				idx = diagMax*boardSize + i;
				if(board[idx] != NULL)
				{
					alive2 = FALSE;
					if(board[idx]->isWhite() != self->isWhite())
						moves[x++] = idx;
					else
						self->m_selfRank += board[idx]->getRank();
				}
				else
					moves[x++] = idx;
			}
			else
				alive2 = FALSE;
		}
		if(alive3)
		{
			diagMin--;
			if(diagMin >= 0)
			{
				idx = diagMin*boardSize + i;
				if(board[idx] != NULL)
				{
					alive3 = FALSE;
					if(board[idx]->isWhite() != self->isWhite())
						moves[x++] = idx;
					else
						self->m_selfRank += board[idx]->getRank();
				}
				else
					moves[x++] = idx;
			}
			else
				alive3 = FALSE;
		}
	}

	alive1 = alive2 = alive3 = TRUE;
	diagMin = diagMax = file;

	for(i = rank-1; i >= 0; i--)
	{
		if(alive1)
		{
			idx = file*boardSize+i;
			if(board[idx] != NULL)
			{
				alive1 = FALSE;
				if(board[idx]->isWhite() != self->isWhite())
					moves[x++] = idx;
				else
					self->m_selfRank += board[idx]->getRank();
			}
			else
				moves[x++] = idx;
		}
		if(alive2)
		{
			diagMax++;
			if(diagMax < boardSize)
			{
				idx = diagMax*boardSize + i;
				if(board[idx] != NULL)
				{
					alive2 = FALSE;
					if(board[idx]->isWhite() != self->isWhite())
						moves[x++] = idx;
					else
						self->m_selfRank += board[idx]->getRank();
				}
				else
					moves[x++] = idx;
			}
			else
				alive2 = FALSE;
		}
		if(alive3)
		{
			diagMin--;
			if(diagMin >= 0)
			{
				idx = diagMin*boardSize + i;
				if(board[idx] != NULL)
				{
					alive3 = FALSE;
					if(board[idx]->isWhite() != self->isWhite())
						moves[x++] = idx;
					else
						self->m_selfRank += board[idx]->getRank();
				}
				else
					moves[x++] = idx;
			}
			else
				alive3 = FALSE;
		}
	}

	for(i = file+1; i < boardSize; i++)
	{
		idx = boardSize*i + rank;
		if(board[idx] != NULL)
		{
			if(board[idx]->isWhite() != self->isWhite())
				moves[x++] = idx;
			else
				self->m_selfRank += board[idx]->getRank();
			break;
		}
		else
			moves[x++] = idx;
	}

	for(i = file-1; i >= 0; i--)
	{
		idx = boardSize*i + rank;
		if(board[idx] != NULL)
		{
			if(board[idx]->isWhite() != self->isWhite())
				moves[x++] = idx;
			else
				self->m_selfRank += board[idx]->getRank();
			break;
		}
		else
			moves[x++] = idx;
	}
	return dontEmbarrassMyself(self, moves, x);
}


int movesKing(chessPiece *self, chessPiece **board, int *moves)
{
	if(moves == NULL)
		return 0;
	if(self->m_boardPos < 0)
		return 0;

	int boardSize = self->m_pBoard->size;
	int x = 0;
	int idx;
	int file = self->m_boardPos / boardSize;
	int rank = self->m_boardPos - (file * boardSize);

	idx = boardSize*(file-1) + (rank-1);
	if(file-1 >= 0 && rank-1 >= 0)
	{
		if(board[idx] != NULL)
		{
			if(board[idx]->isWhite() != self->isWhite())
				moves[x++] = idx;
			else
				self->m_selfRank += board[idx]->getRank();
		}
		else
			moves[x++] = idx;
	}
	idx = boardSize*(file-1) + rank;
	if(file-1 >= 0)
	{
		if(board[idx] != NULL)
		{
			if(board[idx]->isWhite() != self->isWhite())
				moves[x++] = idx;
			else
				self->m_selfRank += board[idx]->getRank();
		}
		else
			moves[x++] = idx;
	}
	idx = boardSize*(file-1) + (rank+1);
	if(file-1 >= 0 && rank+1 < boardSize)
	{
		if(board[idx] != NULL)
		{
			if(board[idx]->isWhite() != self->isWhite())
				moves[x++] = idx;
			else
				self->m_selfRank += board[idx]->getRank();
		}
		else
			moves[x++] = idx;
	}
	idx = boardSize*file + (rank-1);
	if(rank-1 >= 0)
	{
		if(board[idx] != NULL)
		{
			if(board[idx]->isWhite() != self->isWhite())
				moves[x++] = idx;
			else
				self->m_selfRank += board[idx]->getRank();
		}
		else
			moves[x++] = idx;
	}
	idx = boardSize*file + (rank+1);
	if(rank+1 < boardSize)
	{
		if(board[idx] != NULL)
		{
			if(board[idx]->isWhite() != self->isWhite())
				moves[x++] = idx;
			else
				self->m_selfRank += board[idx]->getRank();
		}
		else
			moves[x++] = idx;
	}
	idx = boardSize*(file+1) + (rank-1);
	if(file+1 < boardSize && rank-1 >= 0)
	{
		if(board[idx] != NULL)
		{
			if(board[idx]->isWhite() != self->isWhite())
				moves[x++] = idx;
			else
				self->m_selfRank += board[idx]->getRank();
		}
		else
			moves[x++] = idx;
	}
	idx = boardSize*(file+1) + rank;
	if(file+1 < boardSize)
	{
		if(board[idx] != NULL)
		{
			if(board[idx]->isWhite() != self->isWhite())
				moves[x++] = idx;
			else
				self->m_selfRank += board[idx]->getRank();
		}
		else
			moves[x++] = idx;
	}
	idx = boardSize*(file+1) + (rank+1);
	if(file+1 < boardSize && rank+1 < boardSize)
	{
		if(board[idx] != NULL)
		{
			if(board[idx]->isWhite() != self->isWhite())
				moves[x++] = idx;
			else
				self->m_selfRank += board[idx]->getRank();
		}
		else
			moves[x++] = idx;
	}
	if(self->m_nMoves == 0)
	{
		// king side castling for white (O-O)
		if(self->isWhite() &&
				board[boardSize-1] != NULL &&
				board[boardSize-1]->m_nMoves == 0 &&
				board[boardSize-2] == NULL &&
				board[boardSize-3] == NULL)
		{
			moves[x] = boardSize-2;
			moves[x] |= PIECE_FLAG_CASTLING;
			x++;
		}
		// queen side castling for white (O-O-O)
		if(self->isWhite() &&
				board[0] != NULL &&
				board[0]->m_nMoves == 0 &&
				board[1] == NULL &&
				board[2] == NULL &&
				board[3] == NULL)
		{
			moves[x] = 2;
			moves[x] |= PIECE_FLAG_CASTLING;
			x++;
		}
		// king side castling for black (O-O)
		if(!self->isWhite() &&
				board[63] != NULL &&
				board[63]->m_nMoves == 0 &&
				board[62] == NULL &&
				board[61] == NULL)
		{
			moves[x] = 62;
			moves[x] |= PIECE_FLAG_CASTLING;
			x++;
		}
		// queen side castling for black (O-O-O)
		if(!self->isWhite() &&
				board[56] != NULL &&
				board[56]->m_nMoves == 0 &&
				board[57] == NULL &&
				board[58] == NULL &&
				board[59] == NULL)
		{
			moves[x] = 58;
			moves[x] |= PIECE_FLAG_CASTLING;
			x++;
		}
	}
	return dontEmbarrassMyself(self, moves, x);
}

int movesBishop(chessPiece *self, chessPiece **board, int *moves)
{
	if(moves == NULL)
		return 0;

	int boardSize = self->m_pBoard->size;
	int i;
	int x = 0;
	int idx;
	int file = self->m_boardPos / boardSize;
	int rank = self->m_boardPos - (file * boardSize);
	int diagMax = file;
	int diagMin = file;
	bool alive1, alive2;
	alive1 = alive2 = TRUE;

	for(i = rank+1; i < boardSize; i++)
	{
		diagMax++;
		diagMin--;
		if(alive1 && diagMax < boardSize)
		{
			idx = diagMax*boardSize + i;
			if(board[idx] != NULL)
			{
				alive1 = FALSE;
				if(board[idx]->isWhite() != self->isWhite())
					moves[x++] = idx;
				else
					self->m_selfRank += board[idx]->getRank();
			}
			else
				moves[x++] = idx;
		}
		if(alive2 && diagMin >= 0)
		{
			idx = diagMin*boardSize + i;
			if(board[idx] != NULL)
			{
				alive2 = FALSE;
				if(board[idx]->isWhite() != self->isWhite())
					moves[x++] = idx;
				else
					self->m_selfRank += board[idx]->getRank();
			}
			else
				moves[x++] = idx;
		}
	}

	alive1 = alive2 = TRUE;
	diagMin = diagMax = file;

	for(i = rank-1; i >= 0; i--)
	{
		diagMin--;
		diagMax++;
		if(alive1 && diagMax < boardSize)
		{
			idx = diagMax*boardSize + i;
			if(board[idx] != NULL)
			{
				alive1 = FALSE;
				if(board[idx]->isWhite() != self->isWhite())
					moves[x++] = idx;
				else
					self->m_selfRank += board[idx]->getRank();
			}
			else
				moves[x++] = idx;
		}
		if(alive2 && diagMin >= 0)
		{
			idx = diagMin*boardSize + i;
			if(board[idx] != NULL)
			{
				alive2 = FALSE;
				if(board[idx]->isWhite() != self->isWhite())
					moves[x++] = idx;
				else
					self->m_selfRank += board[idx]->getRank();
			}
			else
				moves[x++] = idx;
		}
	}
	return dontEmbarrassMyself(self, moves, x);
}

int movesKnight(chessPiece *self, chessPiece **board, int *moves)
{
	if(moves == NULL)
		return 0;

	int boardSize = self->m_pBoard->size;
	int idx;
	int x = 0;
	int file = self->m_boardPos / boardSize;
	int rank = self->m_boardPos - (file * boardSize);

	if(file-2 >= 0 && rank-1 >= 0)
	{
		idx = boardSize*(file-2)+(rank-1);
		if(board[idx] != NULL)
		{		
			if(board[idx]->isWhite() != self->isWhite())
				moves[x++] = idx;
			else
				self->m_selfRank += board[idx]->getRank();
		}
		else
			moves[x++] = idx;
	}
	if(file-2 >= 0 && rank+1 < boardSize)
	{
		idx = boardSize*(file-2)+(rank+1);
		if(board[idx] != NULL)
		{		
			if(board[idx]->isWhite() != self->isWhite())
				moves[x++] = idx;
			else
				self->m_selfRank += board[idx]->getRank();
		}
		else
			moves[x++] = idx;
	}
	if(file-1 >= 0 && rank+2 < boardSize)
	{
		idx = boardSize*(file-1)+(rank+2);
		if(board[idx] != NULL)
		{		
			if(board[idx]->isWhite() != self->isWhite())
				moves[x++] = idx;
			else
				self->m_selfRank += board[idx]->getRank();
		}
		else
			moves[x++] = idx;
	}
	if(file+1 < boardSize && rank+2 < boardSize)
	{
		idx = boardSize*(file+1)+(rank+2);
		if(board[idx] != NULL)
		{		
			if(board[idx]->isWhite() != self->isWhite())
				moves[x++] = idx;
			else
				self->m_selfRank += board[idx]->getRank();
		}
		else
			moves[x++] = idx;
	}
	if(file+2 < boardSize && rank+1 < boardSize)
	{
		idx = boardSize*(file+2)+(rank+1);
		if(board[idx] != NULL)
		{		
			if(board[idx]->isWhite() != self->isWhite())
				moves[x++] = idx;
			else
				self->m_selfRank += board[idx]->getRank();
		}
		else
			moves[x++] = idx;
	}
	if(file+2 < boardSize && rank-1 >= 0)
	{
		idx = boardSize*(file+2)+(rank-1);
		if(board[idx] != NULL)
		{		
			if(board[idx]->isWhite() != self->isWhite())
				moves[x++] = idx;
			else
				self->m_selfRank += board[idx]->getRank();
		}
		else
			moves[x++] = idx;
	}
	if(file+1 < boardSize && rank-2 >= 0)
	{
		idx = boardSize*(file+1)+(rank-2);
		if(board[idx] != NULL)
		{		
			if(board[idx]->isWhite() != self->isWhite())
				moves[x++] = idx;
			else
				self->m_selfRank += board[idx]->getRank();
		}
		else
			moves[x++] = idx;
	}
	if(file-1 >= 0 && rank-2 >= 0)
	{
		idx = boardSize*(file-1)+(rank-2);
		if(board[idx] != NULL)
		{		
			if(board[idx]->isWhite() != self->isWhite())
				moves[x++] = idx;
			else
				self->m_selfRank += board[idx]->getRank();
		}
		else
			moves[x++] = idx;
	}
	return dontEmbarrassMyself(self, moves, x);
}

int movesRook(chessPiece *self, chessPiece **board, int *moves)
{
	if(moves == NULL)
		return 0;

	int boardSize = self->m_pBoard->size;
	int i;
	int x = 0;
	int idx;
	int file = self->m_boardPos / boardSize;
	int rank = self->m_boardPos - (file * boardSize);

	for(i = rank+1; i < boardSize; i++)
	{
		idx = file*boardSize+i;
		if(board[idx] != NULL)
		{
			if(board[idx]->isWhite() != self->isWhite())
				moves[x++] = idx;
			else
				self->m_selfRank += board[idx]->getRank();
			break;
		}
		else
			moves[x++] = idx;
	}
	for(i = rank-1; i >= 0; i--)
	{
		idx = file*boardSize+i;
		if(board[idx] != NULL)
		{
			if(board[idx]->isWhite() != self->isWhite())
				moves[x++] = idx;
			else
				self->m_selfRank += board[idx]->getRank();
			break;
		}
		else
			moves[x++] = idx;
	}

	for(i = file+1; i < boardSize; i++)
	{
		idx = boardSize*i + rank;
		if(board[idx] != NULL)
		{
			if(board[idx]->isWhite() != self->isWhite())
				moves[x++] = idx;
			else
				self->m_selfRank += board[idx]->getRank();
			break;
		}
		else
			moves[x++] = idx;
	}

	for(i = file-1; i >= 0; i--)
	{
		idx = boardSize*i + rank;
		if(board[idx] != NULL)
		{
			if(board[idx]->isWhite() != self->isWhite())
				moves[x++] = idx;
			else
				self->m_selfRank += board[idx]->getRank();
			break;
		}
		else
			moves[x++] = idx;
	}
	return dontEmbarrassMyself(self, moves, x);
}

int movesPawn(chessPiece *self, chessPiece **board, int *moves)
{
	if(moves == NULL)
		return 0;

	int boardSize = self->m_pBoard->size;
	int x = 0;
	int idx;
	int file = self->m_boardPos / boardSize;
	int rank = self->m_boardPos - (file * boardSize);

	if(self->isWhite())
	{
		if(file+1 >= boardSize)
			return 0;

		idx = boardSize*(file+1)+rank;
		if(board[idx] == NULL)
			moves[x++] = idx;

		if(rank-1 >= 0)
		{
			idx = boardSize*(file+1)+(rank-1);
			if(board[idx] != NULL)
			{
				if(!board[idx]->isWhite())
					moves[x++] = idx;
				else
					self->m_selfRank += board[idx]->getRank();
			}
		}

		if(rank+1 < boardSize)
		{
			idx = boardSize*(file+1)+(rank+1);
			if(board[idx] != NULL)
			{
				if(!board[idx]->isWhite())
					moves[x++] = idx;
				else
					self->m_selfRank += board[idx]->getRank();
			}
		}

		idx = boardSize*2 + rank;
		if(file == 1 && board[idx] == NULL)
		{
			idx = (boardSize*3)+rank;
			if(board[idx] == NULL)
				moves[x++] = idx;
		}

		//
		// En Passant logic
		//
		if(file == 4)
		{
			chessPiece *piece;

			if(rank > 0)
			{
				idx = file*boardSize + rank - 1;
				piece = board[idx];

				if(piece != NULL &&
				   piece->getId() == ID_BLACK_PAWN &&
				   piece->m_nMoves == 1 &&
				   piece->m_moveNum[1] > self->m_moveNum[self->m_nMoves])
				{
					idx = (file+1)*boardSize + rank - 1;
					if(board[idx] == NULL)
					{
						moves[x] = idx;
						moves[x] |= PIECE_FLAG_EN_PASSANT;
						x++;
					}
				}
			}
			if(rank < boardSize-1)
			{
				idx = file*boardSize + rank + 1;
				piece = board[idx];

				if(piece != NULL &&
				   piece->getId() == ID_BLACK_PAWN &&
				   piece->m_nMoves == 1 &&
				   piece->m_moveNum[1] > self->m_moveNum[self->m_nMoves])
				{
					idx = (file+1)*boardSize + rank + 1;
					if(board[idx] == NULL)
					{
						moves[x] = idx;
						moves[x] |= PIECE_FLAG_EN_PASSANT;
						x++;
					}
				}
			}
		}
	}
	else
	{
		if(file-1 < 0)
			return 0;

		idx = boardSize*(file-1)+rank;
		if(board[idx] == NULL)
			moves[x++] = idx;

		if(rank-1 >= 0)
		{
			idx = boardSize*(file-1)+(rank-1);
			if(board[idx] != NULL)
			{
				if(board[idx]->isWhite())
					moves[x++] = idx;
				else
					self->m_selfRank += board[idx]->getRank();
			}
		}

		if(rank+1 < boardSize)
		{
			idx = boardSize*(file-1)+(rank+1);
			if(board[idx] != NULL)
			{
				if(board[idx]->isWhite())
					moves[x++] = idx;
				else
					self->m_selfRank += board[idx]->getRank();
			}
		}

		idx = boardSize*5 + rank;
		if(file == 6 && board[idx] == NULL)
		{
			idx = (boardSize*4)+rank;
			if(board[idx] == NULL)
				moves[x++] = idx;
		}

		//
		// En Passant logic
		//
		if(file == 3)
		{
			chessPiece *piece;

			if(rank > 0)
			{
				idx = file*boardSize + rank - 1;
				piece = board[idx];

				if(piece != NULL &&
				   piece->getId() == ID_WHITE_PAWN &&
				   piece->m_nMoves == 1 &&
				   piece->m_moveNum[1] > self->m_moveNum[self->m_nMoves])
				{
					idx = (file-1)*boardSize + rank - 1;
					if(board[idx] == NULL)
					{
						moves[x] = idx;
						moves[x] |= PIECE_FLAG_EN_PASSANT;
						x++;
					}
				}
			}
			if(rank+1 < boardSize)
			{
				idx = file*boardSize + rank + 1;
				piece = board[idx];

				if(piece != NULL &&
				   piece->getId() == ID_WHITE_PAWN &&
				   piece->m_nMoves == 1 &&
				   piece->m_moveNum[1] > self->m_moveNum[self->m_nMoves])
				{
					idx = (file-1)*boardSize + rank + 1;
					if(board[idx] == NULL)
					{
						moves[x] = idx;
						moves[x] |= PIECE_FLAG_EN_PASSANT;
						x++;
					}
				}
			}
		}
	}
	return x;
}

